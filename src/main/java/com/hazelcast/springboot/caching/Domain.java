package com.hazelcast.springboot.caching;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(schema = "common", name = "DOMAINS")
public class Domain {

  @Id
  @Column(name = "domain_id", nullable = false, unique = true)
  private Long id;


  @Column(name = "name", nullable = false, unique = true)
  private String name;


  @Column(name = "description", nullable = false)
  private String description;

  @Column(name = "created_ts", nullable = false)
  private String creationDate;

  @Column(name = "updated_ts")
  private String updateDate;


  @OneToMany(mappedBy = "domain", fetch = FetchType.EAGER)
  private Set<Gateway> gateways;

}


