package com.hazelcast.springboot.caching;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(schema = "common", name = "domain_attributes")
public class GatewayAttribute {

  @Id
  @Column(name = "domain_attribute_id", nullable = false)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "domain_gateway_id", nullable = false)
  private Gateway gateway;


  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "value", nullable = false)
  private String value;

  @Column(name = "created_ts", nullable = false)
  private String creationDate;

  @Column(name = "updated_ts")
  private String updateDate;
}


