package com.hazelcast.springboot.caching;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RestResponseDto {

  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  private OffsetDateTime timestamp;
  private Integer status;
  private String error;
  private String message;
  private String messageKey;

  public RestResponseDto() {
    this.timestamp = OffsetDateTime.now(ZoneOffset.UTC);
  }


  public RestResponseDto(Integer status, String error, String message) {
    this();
    this.status = status;
    this.error = error;
    this.message = message;
  }


  public RestResponseDto(Integer status, String error, String message, String messageKey) {
    this(status, error, message);
    this.messageKey = messageKey;
  }



}
