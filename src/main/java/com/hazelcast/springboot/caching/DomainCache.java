package com.hazelcast.springboot.caching;

import java.io.Serializable;
import java.util.Map;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DomainCache implements Serializable {

  private static final long serialVersionUID = -1894621203163819360L;

  private String name;
  private String gateway;
  private Map<String, String> properties;

}
