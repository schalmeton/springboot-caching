package com.hazelcast.springboot.caching;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GatewayRepository extends JpaRepository<Gateway, Long> {

  /**
   * retrieve sub domain active for the given domain name
   * 
   * @param domainName the domain name
   * @return the sub domain active corresponding to the given domain name if exist.
   */
  Optional<Gateway> findByDomainNameIgnoreCaseAndActiveTrue(String domainName);


  Optional<List<Gateway>> findByActiveTrue();
}


