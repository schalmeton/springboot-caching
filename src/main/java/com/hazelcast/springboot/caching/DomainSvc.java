package com.hazelcast.springboot.caching;

public interface DomainSvc {

  /**
   * Retrieve the active properties for the given domain name
   * 
   * @param domainName the name of the domain
   * @return <code>null</code> if not domain corresponding to the given name
   */
  DomainCache retrieveDomainProperties(String domainName);

  String getBookNameByIsbn(String isbn);


}
