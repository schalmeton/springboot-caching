package com.hazelcast.springboot.caching;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public class MapperSvcImpl implements MapperSvc {


  @Override
  public DomainDto toDto(DomainCache domainCache) {
    if (domainCache != null) {
      DomainDto dto = new DomainDto();

      dto.setName(domainCache.getName());
      dto.setGateway(domainCache.getGateway());
      dto.setProperties(domainCache.getProperties());
      return dto;
    } else {
      return null;
    }
  }

  @Override
  public DomainCache toCacheEntity(Gateway entity) {
    if (entity != null) {
      DomainCache dto = new DomainCache();

      dto.setName(entity.getDomain().getName());
      dto.setGateway(entity.getName());

      dto.setProperties(toMap(entity.getAttributes()));
      return dto;
    } else {
      return null;
    }
  }



  private Map<String, String> toMap(Set<GatewayAttribute> entities) {
    Map<String, String> dto = new HashMap<>();
    if (entities != null) {
      for (GatewayAttribute attr : entities) {
        dto.put(attr.getName(), attr.getValue());
      }
    }
    return dto;
  }
}
