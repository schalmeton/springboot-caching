package com.hazelcast.springboot.caching;

import java.util.Properties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import com.hazelcast.config.ClasspathYamlConfig;
import com.hazelcast.config.Config;

@Configuration
public class CacheConfig {


  @Bean
  public Config config(Environment environment) {
    Properties props = new Properties();
   
    props.put("hz.cluster-name", environment.getProperty("hz.cluster-name"));
    props.put("hz.network.port.auto-increment", environment.getProperty("hz.network.port.auto-increment"));
    props.put("hz.network.port_port-count", environment.getProperty("hz.network.port_port-count"));
    props.put("hz.network.port_port", environment.getProperty("hz.network.port_port"));
    props.put("hz.network.join.multicast.enabled", environment.getProperty("hz.network.join.multicast.enabled"));
    props.put("hz.network.join.multicast.multicast-group", environment.getProperty("hz.network.join.multicast.multicast-group"));
    props.put("hz.network.join.multicast.multicast-port", environment.getProperty("hz.network.join.multicast.multicast-port"));
    props.put("hz.map.default.time-to-live-seconds", environment.getProperty("hz.map.default.time-to-live-seconds"));
     
    
    return new ClasspathYamlConfig("hazelcast-2.yaml", props);
  }
}
