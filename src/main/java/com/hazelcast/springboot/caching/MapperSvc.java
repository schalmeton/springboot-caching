package com.hazelcast.springboot.caching;

/**
 * service providing functionalities to map entity to DTO or cache object
 *
 */
public interface MapperSvc {


  /**
   * converts the given entity into a {@link DomainCache} instance, representing object stored in
   * the cache
   * 
   * @param entity the entity to convert
   * @return the entity converted in {@link DomainCache}, can be <code>null</code> if the given
   *         parameter is <code>null</code>
   */
  public DomainCache toCacheEntity(Gateway entity);


  /**
   * converts the given cache object into a {@link DomainDto} instance.
   * 
   * @param domainCache the object to convert
   * @return the cache object converted in {@link DomainDto}, can be <code>null</code> if the given
   *         parameter is <code>null</code>
   */
  public DomainDto toDto(DomainCache domainCache);


}
