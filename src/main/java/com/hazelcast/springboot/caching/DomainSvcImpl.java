package com.hazelcast.springboot.caching;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DomainSvcImpl implements DomainSvc {


  @Autowired
  private GatewayRepository gatewayRepository;

  @Autowired
  private MapperSvc mapperSvc;


  @Override
//  @Cacheable(cacheResolver = "customCacheResolver", key = "#domainName.toUpperCase()",
//      unless = "#result == null")
  @Cacheable(cacheNames = "domains", key = "#domainName.toUpperCase()",
  unless = "#result == null")
public DomainCache retrieveDomainProperties(String domainName) {

    log.info("Details not stored in cache, retrieving information from the for domain name:{}",
        domainName);
    
    Gateway gateway =
        this.gatewayRepository.findByDomainNameIgnoreCaseAndActiveTrue(domainName).orElse(null);

    return mapperSvc.toCacheEntity(gateway);
  }

  @Cacheable("books")
  public String getBookNameByIsbn(String isbn) {
    
    System.out.println("##################################");
    System.out.println("              EXECUTED            ");
    System.out.println("##################################");
    
    
      return findBookInSlowSource(isbn);
  }

  private String findBookInSlowSource(String isbn) {
      // some long processing
      try {
          Thread.sleep(3000);
      } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
          throw new RuntimeException(e);
      }
      return "Sample Book Name";
  }

}
