package com.hazelcast.springboot.caching;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
// @RequestMapping("/")
public class BookController {

  private final BookService bookService;
  private final DomainSvc domainSvc;
  private final MapperSvc mapperSvc;

  BookController(BookService bookService, DomainSvc domainSvc,MapperSvc mapperSvc) {
    this.bookService = bookService;
    this.domainSvc = domainSvc;
    this.mapperSvc = mapperSvc;
  }

  @GetMapping("/domain/{isbn}")
  public ResponseEntity<DomainDto> getDomain(@PathVariable("isbn") String isbn) {

    DomainDto result = mapperSvc.toDto(this.domainSvc.retrieveDomainProperties(isbn));

    if (result == null) {
      result = new DomainDto();
      result.setStatus(HttpStatus.NOT_FOUND.value());
      return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);

    } else {
      result.setStatus(HttpStatus.OK.value());
      return new ResponseEntity<>(result, HttpStatus.OK);

    }

  }

  @GetMapping("/books/{isbn}")
  public String getBookNameByIsbn(@PathVariable("isbn") String isbn) {

    return domainSvc.getBookNameByIsbn(isbn);
  }
}

