package com.hazelcast.springboot.caching;

import java.util.Map;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DomainDto extends RestResponseDto{

  private String name;
  private String gateway;
  private Map<String, String> properties;

  
}
