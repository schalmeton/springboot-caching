package com.hazelcast.springboot.caching;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(schema = "common", name = "DOMAIN_GATEWAYS")
public class Gateway {

  @Id
  @Column(name = "domain_gateway_id", nullable = false, unique = true)
  private Long id;


  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "description", nullable = false)
  private String description;


  @Column(name = "active", nullable = false)
  private Boolean active;


  @Column(name = "created_ts", nullable = false)
  private String creationDate;

  @Column(name = "updated_ts")
  private String updateDate;


  @ManyToOne
  @JoinColumn(name = "domain_id", nullable = false)
  private Domain domain;


  @OneToMany(mappedBy = "gateway", fetch = FetchType.EAGER)
  private Set<GatewayAttribute> attributes;
}


